import itertools
import math
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(a,n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(a,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def prime_permutations():
    primes = Prime_Number_In_Range(1000, 3340)
    solutions = []
    for prime in primes:
        primes_permut = list(map("".join,itertools.permutations(str(prime))))
        if is_prime(prime + 3330) and str((prime+3330)) in primes_permut:
            if is_prime(prime + 6660) and str((prime+6660)) in primes_permut:
                solutions.append(str(prime)+ str(prime + 3330) + str(prime + 6660))
    return solutions

print(prime_permutations())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )